---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...

# Overview


This practical exercise focuses on the search string and
inclusion/exclusion criteria components of the coursework. You will
submit your research questions using Git, so it is important that you
understand how to use Git to commit and upload files to Bitbucket from
the command line. Using the Bitbucket web interface is not acceptable
and will result in reduced marks for the "Research Infrastructure"
component of the coursework.

# Instructions


#.  Read Section 6.1 "Identification of Research" of Kitchenham &
    Charters' "Guidelines for performing systematic literature reviews
    in software engineering".

#.  Read Part B, item 2 "Search string and review protocol" of the
    coursework specification, to be sure you understand the requirements
    for this part of the coursework.

#.  Clone your group's Bitbucket repository.

#.  Edit _research_question.yml_ as required to revise your research question if needed.

#.  Create a search string to search the [IEEEXplore digital library](https://ieeexplore.ieee.org).

#. _Test_ your search string using the "Command Search" text box on
 the
 [Advanced Search](https://ieeexplore.ieee.org/search/advanced/command)
 page.  Be sure:
 
     #. The _syntax_ is correct.
     #. The _semantics_ (meaning) are correct: the search string should
       identifies papers that would answer your research question.
     #. The search identifies more than 30 papers, but not much more
       than 130.

#. Create a list of three  to five _inclusion_ criteria that define
 the papers that will be included in your search.

#. Create a list of one to five _exclusion_ criteria.  Remember that
 exclusion criteria _refine_ the list of papers that pass the
 inclusion criteria; as such, they are _not_ just the negation of your
 _inclusion_ criteria.

#.  Copy _review_protocol.yml_ from this repository to your group workspace.


#.  Open your _review_protocol.yml_ in a text editor and enter the required data as follows:

    1. Write your group name (as it appears on Canvas) in the `group: ` field.

    1.  Copy your _tested_ search string from the "Command Search"
    text box to the  `search_string: ` field.  Be sure the
    string is on _one_ line (or use correct YAML multi-line string syntax).

    2.  Execute your search, then write the number of papers it finds
    in the `num_papers: ` field.
    
        Optionally, save the results of the search in a file called
    #filename(papers.csv).  You will need this for the next deliverable.
    
    3.  Write your inclusion criteria, one per line, after the hyphens
    ('-') following the `inclusion_criteria:` line.  If you have less than
    five criteria, delete the remaining hyphens.  If you have more
    than five criteria, add more hyphens.

    4.  Write your exclusion criteria, one per line, after the hyphens ('-')
    following the `exclusion_criteria:` line.  If you have less than
    three criteria, delete the remaining hyphens.  If you have more
    than three criteria, add more hyphens.


#. _Spellcheck^[Notepad has a spellcheck plugin; install and _use_ it.]_,_Proofread_,
 then commit your #filename(review_protocol.yml) file (and
 #filename(papers.csv) file if present).
 
#. _Validate_ your #filename(review_protocol.yml) file using
 [YAML Lint](http://www.yamllint.com/).  Be sure _all_ fields are
 displayed correctly.


#.  Correct any errors, then push your workspace to Bitbucket by 23:59 #cw_protocol_due.


_Note:_ you _must_ update your #filename(research_question.yml) file
if you have changed your research question: we will evaluate the
quality of your search string based on how well it addresses your
research question.
